# AFINADOR_AUTOMATICO
Aluno: Vitor José Duarte Quintans (18201357);
Professor: Wyllian Bezerra da Silva;
Curso: EMB5636;
Universidade Federal de Santa Catarina.

Resumo — Este   projeto   tem   como   objetivo   desenvolver   um dispositivo capaz de afinar um instrumento de corda automaticamente, detectando a frequência do som produzido e aumentando ou diminuindo a tensão da corda, com uso de um motor,  até que se  atinja  uma  frequência  de  referência.

Utiliza o _Template_ em ![\LaTeX](https://render.githubusercontent.com/render/math?math=%5CLaTeX) para elaboração de trabalhos acadêmicos da [Universidade Federal de Santa Catarina (UFSC)](http://ufsc.br/), adaptado pelo [Prof. Wyllian Bezerra da Silva](http://wyllian.prof.ufsc.br/), a partir dos _templates_ disponibilizados pelo [IEEE](https://journals.ieeeauthorcenter.ieee.org/create-your-ieee-journal-article/authoring-tools-and-templates/ieee-article-templates/templates-for-transactions).
