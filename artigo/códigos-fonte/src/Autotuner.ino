//Auto tuner 
//by Vitor Quintans
//uses a adapted version of generalized wave freq detection 
//with 38.5kHz sampling rate and interrupts
//by Amanda Ghassaei
//https://www.instructables.com/id/Arduino-Frequency-Detection/
//Sept 2021

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
*/


#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#define REFERENCE_VOLTAGE 63

#define TEST //used to test the implementation
#define ALPHA //start to use the alpha value 

//clipping indicator variables
boolean clipping = 0;

//data storage variables
byte newData = 0;
byte prevData = 0;
unsigned int time = 0;//keeps time and sends vales to store in timer[] occasionally
int timer[10];//sstorage for timing of events
int slope[10];//storage for slope of events
unsigned int totalTimer;//used to calculate period
unsigned int period;//storage for period of wave
byte index = 0;//current storage index
float frequency;//storage for frequency calculations
int maxSlope = 0;//used to calculate max slope as trigger point
int newSlope;//storage for incoming slope data

//variables for decided whether you have a match
byte noMatch = 0;//counts how many non-matches you've received to reset variables if it's been too long
byte slopeTol = 3;//slope tolerance- adjust this if you need
int timerTol = 10;//timer tolerance- adjust this if you need

//variables for amp detection
unsigned int ampTimer = 0;
byte maxAmp = 0;
byte checkMaxAmp;
byte ampThreshold = 40;//raise if you have a very noisy signal

//Display Pins
Adafruit_PCD8544 display = Adafruit_PCD8544(7, 6, 5, 4, 3);

//Motor Pins
const uint8_t motorPin1 =8;
const uint8_t motorPin2 =9;
const uint8_t motorPin3 =10;
const uint8_t motorPin4 =11;


//Seletor
const char* cordas_nomes[] =        {"E4", "B3", "G3", "D3", "A2", "E2"};
const float cordas_frequencias[] =  {330,   247,  196,  146 , 110,  82};
#ifdef ALPHA 
const float alpha[] = { 16.7491, 15.2311 , 21.0548, 10.5472, 9.5755, 6.2955}; //insert here yours alphas
float inv_alpha[6];
#endif

uint8_t cordas_index = 5;
const int potentiometerPin = A1;
const byte interruptPin = 2;

volatile uint8_t debounce_wait = 100;
bool button_wait = false;
unsigned long wait_start = 0;

float fdesejada = 220;
float ftol = 3;
bool tunned = false;



void lowerCorda()
{
  if(!button_wait)
  { 
    button_wait = true;
    cordas_index = (cordas_index < 5) ? (cordas_index+1) : (0);
    
    wait_start = millis();
    tunned = false;
    display.clearDisplay();
    display.setTextSize(1);
    display.print("Tuning: ");
    display.setTextSize(2);
    display.println(cordas_nomes[cordas_index]);
    display.println(frequency);
    display.display();
  }
}

void setup(){
  
  Serial.begin(9600);
  
  pinMode(13,OUTPUT);//led indicator pin
  pinMode(12,OUTPUT);//output pin
  pinMode(motorPin1,OUTPUT);//output pin
  pinMode(motorPin2,OUTPUT);//output pin
  pinMode(motorPin3,OUTPUT);//output pin
  pinMode(motorPin4,OUTPUT);//output pin
  delay(300);
  pinMode(interruptPin, INPUT_PULLUP);
  delay(600);
  attachInterrupt(digitalPinToInterrupt(interruptPin), lowerCorda, RISING );
  delay(200);
  display.begin();
  delay(200);
  // init done
  display.setContrast(75);
   display.setTextSize(2);
  display.setTextColor(BLACK);
  display.setCursor(0,0); 
  cli();//diable interrupts
  
  //set up continuous sampling of analog pin 0 at 38.5kHz
 
  //clear ADCSRA and ADCSRB registers
  ADCSRA = 0;
  ADCSRB = 0;
  
  ADMUX |= (1 << REFS0); //set reference voltage
  ADMUX |= (1 << ADLAR); //left align the ADC value- so we can read highest 8 bits from ADCH register only
  
  ADCSRA |= (1 << ADPS2) | (1 << ADPS0); //set ADC clock with 32 prescaler- 16mHz/32=500kHz
  ADCSRA |= (1 << ADATE); //enabble auto trigger
  ADCSRA |= (1 << ADIE); //enable interrupts when measurement complete
  ADCSRA |= (1 << ADEN); //enable ADC
  ADCSRA |= (1 << ADSC); //start ADC measurements
  
  sei();//enable interrupts
  //-------------------------------------------------------------------------------------------------

 #ifdef ALPHA 
  for(int it = 0; it < 6; it++) inv_alpha[it] = 1.0/alpha[it];
 #endif 

}

ISR(ADC_vect) {//when new ADC value ready
  
  PORTB &= B11101111;//set pin 12 low
  prevData = newData;//store previous value
  newData = ADCH;//get value from A0
  if (prevData < REFERENCE_VOLTAGE && newData >=REFERENCE_VOLTAGE){//if increasing and crossing midpoint
    newSlope = newData - prevData;//calculate slope
    if (abs(newSlope-maxSlope)<slopeTol){//if slopes are ==
      //record new data and reset time
      slope[index] = newSlope;
      timer[index] = time;
      time = 0;
      if (index == 0){//new max slope just reset
        PORTB |= B00010000;//set pin 12 high
        noMatch = 0;
        index++;//increment index
      }
      else if (abs(timer[0]-timer[index])<timerTol && abs(slope[0]-newSlope)<slopeTol){//if timer duration and slopes match
        //sum timer values
        totalTimer = 0;
        for (byte i=0;i<index;i++){
          totalTimer+=timer[i];
        }
        period = totalTimer;//set period
        //reset new zero index values to compare with
        timer[0] = timer[index];
        slope[0] = slope[index];
        index = 1;//set index to 1
        PORTB |= B00010000;//set pin 12 high
        noMatch = 0;
      }
      else{//crossing midpoint but not match
        index++;//increment index
        if (index > 9){
          reset();
        }
      }
    }
    else if (newSlope>maxSlope){//if new slope is much larger than max slope
      maxSlope = newSlope;
      time = 0;//reset clock
      noMatch = 0;
      index = 0;//reset index
    }
    else{//slope not steep enough
      noMatch++;//increment no match counter
      if (noMatch>9){
        reset();
      }
    }
  }
    
  if (newData == 0 || newData == 1023){//if clipping
    PORTB |= B00100000;//set pin 13 high- turn on clipping indicator led
    clipping = 1;//currently clipping
  }
  
  time++;//increment timer at rate of 38.5kHz
  
  ampTimer++;//increment amplitude timer
  if (abs(REFERENCE_VOLTAGE-ADCH)>maxAmp){
    maxAmp = abs(REFERENCE_VOLTAGE-ADCH);
  }
  if (ampTimer==1000){
    ampTimer = 0;
    checkMaxAmp = maxAmp;
    maxAmp = 0;
  }
  
}

void reset(){//clea out some variables
  index = 0;//reset index
  noMatch = 0;//reset match couner
  maxSlope = 0;//reset slope
}


void checkClipping(){//manage clipping indicator LED
  if (clipping){//if currently clipping
    PORTB &= B11011111;//turn off clipping indicator led
    clipping = 0;
  }
}




void SpinMotor(int num_ciclos)
{ 
  if(abs(num_ciclos) > 150) num_ciclos = (num_ciclos/abs(num_ciclos))*150; //protecao inicial 
  if( num_ciclos > 0)
  {
   for ( int i=0 ; i<abs(num_ciclos) ; i=i+1)
   {
    digitalWrite(motorPin1, LOW);
    digitalWrite(motorPin2, LOW);
    digitalWrite(motorPin3, HIGH);
    digitalWrite(motorPin4, HIGH);
    delay(3);
    digitalWrite(motorPin1, LOW);
    digitalWrite(motorPin2, HIGH);
    digitalWrite(motorPin3, HIGH);
    digitalWrite(motorPin4, LOW);
    delay(3);
    digitalWrite(motorPin1, HIGH);
    digitalWrite(motorPin2, HIGH);
    digitalWrite(motorPin3, LOW);
    digitalWrite(motorPin4, LOW);
    delay(3);
    digitalWrite(motorPin1, HIGH);
    digitalWrite(motorPin2, LOW);
    digitalWrite(motorPin3, LOW);
    digitalWrite(motorPin4, HIGH);
    delay(3);
   }
  }
  else
  {
    for ( int i=0 ; i<abs(num_ciclos) ; i=i+1)
    {
       digitalWrite(motorPin1, LOW);
       digitalWrite(motorPin2, LOW);
       digitalWrite(motorPin3, HIGH);
       digitalWrite(motorPin4, HIGH);
       delay(3);
       digitalWrite(motorPin1, HIGH);
       digitalWrite(motorPin2, LOW);
       digitalWrite(motorPin3, LOW);
       digitalWrite(motorPin4, HIGH);
       delay(3);
       digitalWrite(motorPin1, HIGH);
       digitalWrite(motorPin2, HIGH);
       digitalWrite(motorPin3, LOW);
       digitalWrite(motorPin4, LOW);
       delay(3);
       digitalWrite(motorPin1, LOW);
       digitalWrite(motorPin2, HIGH);
       digitalWrite(motorPin3, HIGH);
       digitalWrite(motorPin4, LOW);
       delay(3);
       
    }
 }
       digitalWrite(motorPin1, LOW);
       digitalWrite(motorPin2, LOW);
       digitalWrite(motorPin3, LOW);
       digitalWrite(motorPin4, LOW);
       delay(3);
}

#ifdef TEST
unsigned long StartTime;
float ElapsedTime;
bool waiting_start = true;

void end_experiment();
void untune();
void new_experiment();
int num_experiments = 5;
int count_ex = 0;

#endif




void loop()
{
  
  checkClipping(); 

  fdesejada = cordas_frequencias[cordas_index];
  if (checkMaxAmp>ampThreshold)
  {
    frequency = 38462/float(period);//calculate frequency timer rate/period
    if(frequency < 500 && frequency > 40)
    {
      #ifdef TEST
       if(waiting_start) 
       {
        StartTime = millis();
        waiting_start = false;
       }
      #endif
      
      //PORTB |= B00100000;//set pin 13 high- turn on clipping indicator led
      //print results
      //Serial.print(frequency);
      //Serial.println(" hz");
      display.clearDisplay();
      display.setTextSize(1);
      display.print("Tuning: ");
      display.setTextSize(2);
      display.println(cordas_nomes[cordas_index]);
      display.println(frequency);
      display.display();
      
      if( abs(frequency - fdesejada) > ftol )
      {
        //Serial.print("x: ");
        //Serial.println(fdesejada*fdesejada - frequency*frequency);
     #ifdef ALPHA
        SpinMotor((-1)*(fdesejada*fdesejada - frequency*frequency)*inv_alpha[cordas_index]);
     #else
        SpinMotor(50*(fdesejada - frequency)/abs(fdesejada - frequency));
     
     #endif
        delay(700);
      }
      else
      {
        display.clearDisplay();
        display.println(" tunned!");
        display.display();
        
 #ifndef TEST
        tunned = true;
        while(tunned);
  #else
        end_experiment();
        untune();
        new_experiment();
       
  #endif  
      
      }
    }
    //reset();
  }

  if((millis() - wait_start) > debounce_wait) 
    button_wait = false;

  delay(100);//delete this if you want
  
  //do other stuff here
  }
  
#ifdef TEST
//teste functions
  void new_experiment()
  {
     display.clearDisplay();
     display.print("experiment, strike the string to start couting");
      display.print(count_ex);
       display.print("strike the string to start couting");
     
     display.display(); 
     delay(20);
     if(count_ex >= num_experiments )end_group_experiment();
     else count_ex++;
     waiting_start = true;
       
  }
  void untune()
  {
     display.clearDisplay();
     display.println("Untuning ");
     display.println(" a bit...");
     display.display();
     int reverse = (rand()%2) ? 1 : -1 ;
    for(int i = 0; i < 5; i++) SpinMotor((rand()%151)*reverse);
    
  }
  void end_experiment()
  {
    ElapsedTime = millis() - StartTime;
    Serial.print(ElapsedTime/1000.0);
    Serial.print(",\n");
    display.clearDisplay();
    display.println("Sending Your Data ");
    display.println("To Serial");
    display.display();
    delay(80);
  }

  void end_group_experiment()
  {
    display.clearDisplay();
    display.print("I finish a group of experiments! press the button to start again");
    count_ex = 0;
    while(digitalRead(interruptPin));
    display.display();
  }
#endif
