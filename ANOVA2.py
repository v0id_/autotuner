import pandas as pd
# load data file
df = pd.read_csv("./Tempos de Afinação2.csv")
# reshape the d dataframe suitable for statsmodels package 
#df_melt = pd.melt(df.reset_index(), id_vars=['index'], value_vars=['Afinacao_manual', 'Afinacao_sem_alpha','Afinacao_com_alpha'])
df = df.loc[:, id_vars=['index'], value_vars=['Afinacao_manual', 'Afinacao_sem_alpha','Afinacao_com_alpha']]
# replace column names
df_melt.columns = ['index', 'tratamentos', 'Tempo']

# generate a boxplot to see the data distribution by treatments. Using boxplot, we can 
# easily detect the differences between different treatments
import matplotlib.pyplot as plt
import seaborn as sns
ax = sns.boxplot(x='tratamentos', y='Tempo', data=df_melt, color='#99c2a2')
ax = sns.swarmplot(x="tratamentos", y="Tempo", data=df_melt, color='#7d0013')
plt.show()


import scipy.stats as stats
# stats f_oneway functions takes the groups as input and returns ANOVA F and p value
fvalue, pvalue = stats.f_oneway( df['Afinacao_manual'],df['Afinacao_sem_alpha'], df['Afinacao_com_alpha'])
print(fvalue, pvalue)
# 17.492810457516338 2.639241146210922e-05

# get ANOVA table as R like output
import statsmodels.api as sm
from statsmodels.formula.api import ols

# Ordinary Least Squares (OLS) model
model = ols('Tempo ~ C(tratamentos)', data=df_melt).fit()
anova_table = sm.stats.anova_lm(model, typ=2)
print(anova_table)

#print(df['Afinacao_manual'].mean())

# ANOVA table using bioinfokit v1.0.3 or later (it uses wrapper script for anova_lm)
#from bioinfokit.analys import stat
#res = stat()
#res.anova_stat(df=df_melt, res_var='Tempo', anova_model='Tempo ~ C(treatments)')
#res.anova_summary
