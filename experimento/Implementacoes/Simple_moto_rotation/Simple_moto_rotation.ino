//Simple_motor_rotation
//used to test the step motor
//and calculate alpha
//by Vitor Quintans
//Sept 2021

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
*/
const uint8_t motorPin1 =8;
const uint8_t motorPin2 =9;
const uint8_t motorPin3 =10;
const uint8_t motorPin4 =11;
int ncic =150;

void SpinMotor(int num_ciclos);

void setup() {
  pinMode(motorPin1,OUTPUT);//output pin
  pinMode(motorPin2,OUTPUT);//output pin
  pinMode(motorPin3,OUTPUT);//output pin
  pinMode(motorPin4,OUTPUT);//output pin
  pinMode(2, INPUT_PULLUP);
   
   
}
int reverse = 1;
void loop() {
 while(digitalRead(2)){}
  SpinMotor(ncic*reverse);
  reverse = -1*reverse;
  delay(600);
}

void SpinMotor(int num_ciclos)
{ 
  if(abs(num_ciclos) > 150) num_ciclos = (num_ciclos/abs(num_ciclos))*150; //protecao inicial 
  if( num_ciclos > 0)
  {
   for ( int i=0 ; i<abs(num_ciclos) ; i=i+1)
   {
    digitalWrite(motorPin1, LOW);
    digitalWrite(motorPin2, LOW);
    digitalWrite(motorPin3, HIGH);
    digitalWrite(motorPin4, HIGH);
    delay(3);
    digitalWrite(motorPin1, LOW);
    digitalWrite(motorPin2, HIGH);
    digitalWrite(motorPin3, HIGH);
    digitalWrite(motorPin4, LOW);
    delay(3);
    digitalWrite(motorPin1, HIGH);
    digitalWrite(motorPin2, HIGH);
    digitalWrite(motorPin3, LOW);
    digitalWrite(motorPin4, LOW);
    delay(3);
    digitalWrite(motorPin1, HIGH);
    digitalWrite(motorPin2, LOW);
    digitalWrite(motorPin3, LOW);
    digitalWrite(motorPin4, HIGH);
    delay(3);
   }
  }
  else
  {
    for ( int i=0 ; i<abs(num_ciclos) ; i=i+1)
    {
       digitalWrite(motorPin1, LOW);
       digitalWrite(motorPin2, LOW);
       digitalWrite(motorPin3, HIGH);
       digitalWrite(motorPin4, HIGH);
       delay(3);
       digitalWrite(motorPin1, HIGH);
       digitalWrite(motorPin2, LOW);
       digitalWrite(motorPin3, LOW);
       digitalWrite(motorPin4, HIGH);
       delay(3);
       digitalWrite(motorPin1, HIGH);
       digitalWrite(motorPin2, HIGH);
       digitalWrite(motorPin3, LOW);
       digitalWrite(motorPin4, LOW);
       delay(3);
       digitalWrite(motorPin1, LOW);
       digitalWrite(motorPin2, HIGH);
       digitalWrite(motorPin3, HIGH);
       digitalWrite(motorPin4, LOW);
       delay(3);
    }
 }
       digitalWrite(motorPin1, LOW);
       digitalWrite(motorPin2, LOW);
       digitalWrite(motorPin3, LOW);
       digitalWrite(motorPin4, LOW);
       delay(3);
}
